<?php
$tipo = $_REQUEST["tipo"];
if($tipo == "sumaNormal"){
    $num1 = $_REQUEST["num1"];
    $num2 = $_REQUEST["num2"];
    $resultado = $num1 * $num2;
    echo "<h1>La suma de ambos numeros es: ".$resultado."</h1>";
}
if($tipo == "sumaPrimos"){
    $num1 = $_REQUEST["num1p"];
    $num2 = $_REQUEST["num2p"];
    if(primo($num1)){
        if(primo($num2)){
            $resultado = $num1 * $num2;
            echo "<h1>La suma de ambos numeros primos es: ".$resultado."</h1>";
        }else{
            echo "<h1>El segundo número no es primo</h1>";
        }
    }else{
        echo "<h1>El primer número no es primo</h1>";
    }
}

function primo($num)
{
    if ($num == 2 || $num == 3 || $num == 5 || $num == 7) {
        return True;
    } else {
        if ($num % 2 != 0) {
            for ($i = 3; $i <= sqrt($num); $i += 2) {
                if ($num % $i == 0) {
                    return False;
                }
            }
            return True;
        }
    }
    return False;
}

echo "<a href='index.html'>Regresar</a>";
?>
